const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./testData.json');

    class Client3 {
        static async sendLoginUserRequest(url, args) {
            return fetch(`${url}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: args.registeredEmail,
                    password: args.registeredPassword
                })
            });
        };

        static async sendGetUserId(url, token) {
            return fetch(`${url}/auth/me`, {
                method: 'GET',
                headers: {
                    'Authorization': `${token.data.token_type} ${token.data.access_token}`
                }
            });
        };

        static async sendDeleteUser(url, token, userId) {
            return fetch(`${url}/users/${userId}`, {
                method: 'DELETE',
                headers: {
                    'Authorization': `${token.data.token_type} ${token.data.access_token}`
                }
            });
        };
    
    };

    async function deleteUser(url, args) {

        console.log(`Creating user for ${url}`);
        const responseLoginData = await Client3.sendLoginUserRequest(url, args);

        if (responseLoginData.status === 200) {
            const token = await responseLoginData.json();
            const userMe = await Client3.sendGetUserId(url, token);
            const userData = await userMe.json();
            const userId = await userData.data.id.toString();
            await Client3.sendDeleteUser(url, token, userId);
            return Promise.resolve();
        }
        const responseJSON = await responseLoginData.json();
        const error = new Error(`Failed to successfully create user for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    };

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Deleting User=========');
        deleteUser(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
    });
}

