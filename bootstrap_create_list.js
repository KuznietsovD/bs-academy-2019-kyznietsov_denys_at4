const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./testData.json');

    class Client1 {
        static async sendLoginUserRequest(url, args) {
            return fetch(`${url}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: args.email,
                    password: args.password
                })
            });
        };
        static async sendCreateListRequest(url, token) {
            return fetch(`${url}/user-lists`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `${token.data.token_type} ${token.data.access_token}`
                },
                body: JSON.stringify({
                    name : args.name
                })
            });
        };
    };

    async function createList(url, args) {

        console.log(`Creating list for ${url}`);
        const responseLoginData = await Client1.sendLoginUserRequest(url, args);
        if (responseLoginData.status === 200) {
            const token = await responseLoginData.json();
            await Client1.sendCreateListRequest(url, token);
            return Promise.resolve();
        }

        const responseJSON = await responseLoginData.json();
        const error = new Error(`Failed to successfully create list for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);

    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    };

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Creating list=========');
        createList(url, args)
            .then(() => { })
            .catch(error => {
                done(handleError(error));
            })
    });
}
