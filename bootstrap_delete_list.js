const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./testData.json');

    class Client2 {
        static async sendLoginUserRequest(url, args) {
            return fetch(`${url}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: args.email,
                    password: args.password
                })
            });
        };
        static async sendGetUserId(url, token) {
            return fetch(`${url}/auth/me`, {
                method: 'GET',
                headers: {
                    'Authorization': `${token.data.token_type} ${token.data.access_token}`
                }
            });
        };
        static async sendGetLists(url, token, userId) {
            return fetch(`${url}/users/${userId}/lists`, {
                method: 'GET',
                headers: {
                    'Authorization': `${token.data.token_type} ${token.data.access_token}`
                }
            });
        };
        static async sendDeletUserList(url, token, listId) {
            return fetch(`${url}/user-lists/${listId}`, {
                method: 'DELETE',
                headers: {
                    'Authorization': `${token.data.token_type} ${token.data.access_token}`
                }
            });
        };
    };

    async function deleteList(url, args) {

        console.log(`Delete list for ${url}`);
        const responseLoginData = await Client2.sendLoginUserRequest(url, args);
        if (responseLoginData.status === 200) {
            const token = await responseLoginData.json();
            const userMe = await Client2.sendGetUserId(url, token);
            const userData = await userMe.json();
            const userId = await userData.data.id.toString();
            const responseLists = await Client2.sendGetLists(url, token, userId);
            const lists = await responseLists.json();
            const listId = await lists.data[0].id.toString();
            await Client2.sendDeletUserList(url, token, listId);

            return Promise.resolve();
        }

        const responseJSON = await responseLoginData.json();
        const error = new Error(`Failed to successfully delete list for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);

    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    };

    module.exports = (async done2 => {
        console.log('========Start=========');
        console.log('========Deleting list=========');
        deleteList(url, args)
            .then((x) => { console.log(x) })
            .catch(error => {
                done2(handleError(error));
            })
    });
}


